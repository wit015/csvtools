# csvtools

csvtools is a tool to proces CSV files

## Requirements

For installing the csvtools package we have the following dependencies:
- pandas >= 0.13
- sqlalchemy >= 1.3
- click >= 8.0

## install

Installing csvtools can be done with pip through:
```commandline
pip install csvtools
```


# How to use

```commandline
Usage: python -m csvtools.cmd [OPTIONS] COMMAND [ARGS]...

  With csvtools you can operate on CSV files

Options:
  --help  Show this message and exit.

Commands:
  csv2db

```