# -*- coding: utf-8 -*-
# Copyright (c) 2022, Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), January 2022
"""csvtools is a set of utilities to process CSV files
"""
__version__ = "0.9.0"