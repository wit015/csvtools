# -*- coding: utf-8 -*-
# Copyright (c) 2022, Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), January 2022
import click

from .csv2db import process_csv


@click.group(help="With csvtools you can operate on CSV files")
def cli():
    pass


@click.command("csv2db")
@click.argument("csv_fname", type=click.Path(exists=True))
@click.argument("sqlite_fname", type=click.Path())
def cmd_csv2db(csv_fname, sqlite_fname):
    """Function for the commandline interface for csv2db

    :param csv_fname: the CSV input filename
    :param sqlite_fname: the SQLite output filename

    """
    process_csv(csv_fname, sqlite_fname)


def main():
    cli.add_command(cmd_csv2db)
    cli()


if __name__ == "__main__":
    main()