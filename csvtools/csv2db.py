# -*- coding: utf-8 -*-
# Copyright (c) 2022, Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), January 2022
from pathlib import Path

import pandas as pd
import sqlalchemy as sa


def get_database_engine(sqlite_fname):
    """Generates the SQLalchemy connection to the database

    :param sqlite_fname: the SQLite filename
    :return: a SA database connection
    """

    dsn = f"sqlite:///{sqlite_fname}"
    engine = sa.create_engine(dsn)
    return engine


def process_csv(csv_fname, sqlite_fname):
    """Writes a CSV file into a SQLite database file

    :param csv_fname: Filename of the CSV file
    :param sqlite_fname: Filename of the SQLite database
    :return: a pandas dataframe
    """
    csv_fname = Path(csv_fname).absolute()
    sqlite_fname = Path(sqlite_fname).absolute()

    df = pd.read_csv(csv_fname)
    engine = get_database_engine(sqlite_fname)
    with engine.begin() as DBconn:
        df.to_sql("csv_data", DBconn, if_exists="replace", index=False)

    return df