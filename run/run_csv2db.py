# -*- coding: utf-8 -*-
# Copyright (c) 2022, Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), January 2022
from csvtools.csv2db import process_csv

csv_fname = r"c:\temp\US_UFO_sightings.csv"
sqlite_fname = r"c:\temp\US_UFO_sightings.db"
process_csv(csv_fname, sqlite_fname)
