import unittest
from pathlib import Path
import tempfile

import pandas as pd
import sqlalchemy as sa

from csvtools.csv2db import process_csv, get_database_engine


class TestCSV2DB(unittest.TestCase):

    def runTest(self):
        this_dir = Path(__file__).parent
        test_data = this_dir / "testdata.csv"
        test_db = Path(tempfile.gettempdir()) / "testdata.db"

        df1 = process_csv(test_data, test_db)

        # read back CSV from database
        engine = get_database_engine(test_db)
        df2 = pd.read_sql_table("csv_data", engine)

        self.assertEqual(len(df1), len(df2))
        for row1, row2 in zip(df1.itertuples(), df2.itertuples()):
            self.assertEqual(row1, row2)
